﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPX_Reader
{
    public class OpDescription
    {
        public string Name { get; set; } //<name>

        public string Comment { get; set; } //<cmt>

        public string Description { get; set; } //<desc>

        public string Source { get; set; } //<src>

        public string Url { get; set; } //<url>

        public string UrlName { get; set; } //<urlname>

        public string Symbol { get; set; } //<sym>

        public string Type { get; set; } //<type>
    }
}
