﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPX_Reader
{
    public class GPXinfo
    {
        public string Version { get; set; } //<version>

        public string Creator { get; set; } //<creator>

        public string Name { get; set; } //<name>

        public string Description { get; set; } //<desc>

        public string Author { get; set; } //<author>

        public string Email { get; set; } //<email>

        public string Url { get; set; } //<url>

        public string UrlName { get; set; } //<urlname>

        public string Time { get; set; } //<time>

        public string Keywords { get; set; } //<keywords>

        public string Bounds { get; set; } //<bounds>
    }
}
