﻿namespace GPX_Reader
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.filePath = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Namespace = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.TracksGrid = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblNoDataT4 = new System.Windows.Forms.Label();
            this.trkAccuracyGrid = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblNoDataT3 = new System.Windows.Forms.Label();
            this.trkDescriptionGrid = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblNoDataT1 = new System.Windows.Forms.Label();
            this.TrackInfoGrid = new System.Windows.Forms.DataGridView();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.lblNoDataW3 = new System.Windows.Forms.Label();
            this.wptAccuracyGrid = new System.Windows.Forms.DataGridView();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.lblNoDataW2 = new System.Windows.Forms.Label();
            this.wptDescriptionGrid = new System.Windows.Forms.DataGridView();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.lblNoDataW1 = new System.Windows.Forms.Label();
            this.WaypointsGrid = new System.Windows.Forms.DataGridView();
            this.lblNoDataT2 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lblNoDataR2 = new System.Windows.Forms.Label();
            this.RoutesGrid = new System.Windows.Forms.DataGridView();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lblNoDataR4 = new System.Windows.Forms.Label();
            this.rteAccuracyGrid = new System.Windows.Forms.DataGridView();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.lblNoDataR3 = new System.Windows.Forms.Label();
            this.rteDescriptionGrid = new System.Windows.Forms.DataGridView();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.lblNoDataR1 = new System.Windows.Forms.Label();
            this.RouteInfoGrid = new System.Windows.Forms.DataGridView();
            this.txtVersion = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCreator = new System.Windows.Forms.TextBox();
            this.txtAuthor = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtUrl = new System.Windows.Forms.TextBox();
            this.txtUrlName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.rtxDescription = new System.Windows.Forms.RichTextBox();
            this.txtBounds = new System.Windows.Forms.TextBox();
            this.txtKeywords = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.rtxRaw = new System.Windows.Forms.RichTextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TracksGrid)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trkAccuracyGrid)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trkDescriptionGrid)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrackInfoGrid)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.wptAccuracyGrid)).BeginInit();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.wptDescriptionGrid)).BeginInit();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WaypointsGrid)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RoutesGrid)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rteAccuracyGrid)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rteDescriptionGrid)).BeginInit();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RouteInfoGrid)).BeginInit();
            this.groupBox12.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "File Path:";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("News706 BT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(465, 17);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(119, 37);
            this.button1.TabIndex = 1;
            this.button1.Text = "Browse File";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // filePath
            // 
            this.filePath.Enabled = false;
            this.filePath.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.filePath.Location = new System.Drawing.Point(104, 24);
            this.filePath.Name = "filePath";
            this.filePath.Size = new System.Drawing.Size(355, 23);
            this.filePath.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(21, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Namespace:";
            // 
            // Namespace
            // 
            this.Namespace.Enabled = false;
            this.Namespace.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Namespace.Location = new System.Drawing.Point(124, 59);
            this.Namespace.Name = "Namespace";
            this.Namespace.Size = new System.Drawing.Size(335, 23);
            this.Namespace.TabIndex = 2;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(12, 105);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(932, 616);
            this.tabControl1.TabIndex = 5;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tabPage1.Controls.Add(this.groupBox13);
            this.tabPage1.Controls.Add(this.txtBounds);
            this.tabPage1.Controls.Add(this.txtKeywords);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.groupBox12);
            this.tabPage1.Controls.Add(this.txtUrlName);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.txtAuthor);
            this.tabPage1.Controls.Add(this.txtUrl);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.txtName);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.txtEmail);
            this.tabPage1.Controls.Add(this.txtCreator);
            this.tabPage1.Controls.Add(this.txtDate);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.txtVersion);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(924, 590);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "GPX Info";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tabPage2.Controls.Add(this.groupBox8);
            this.tabPage2.Controls.Add(this.groupBox9);
            this.tabPage2.Controls.Add(this.groupBox10);
            this.tabPage2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(924, 590);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Waypoints";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tabPage3.Controls.Add(this.groupBox7);
            this.tabPage3.Controls.Add(this.groupBox3);
            this.tabPage3.Controls.Add(this.groupBox2);
            this.tabPage3.Controls.Add(this.groupBox1);
            this.tabPage3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(924, 590);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Tracks";
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tabPage4.Controls.Add(this.groupBox4);
            this.tabPage4.Controls.Add(this.groupBox5);
            this.tabPage4.Controls.Add(this.groupBox6);
            this.tabPage4.Controls.Add(this.groupBox11);
            this.tabPage4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(924, 590);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Routes";
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tabPage5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(924, 590);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Map";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.lblNoDataT2);
            this.groupBox7.Controls.Add(this.TracksGrid);
            this.groupBox7.Location = new System.Drawing.Point(5, 121);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(912, 161);
            this.groupBox7.TabIndex = 15;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Trackpoints Required and Position Information";
            // 
            // TracksGrid
            // 
            this.TracksGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.TracksGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TracksGrid.Location = new System.Drawing.Point(13, 21);
            this.TracksGrid.Name = "TracksGrid";
            this.TracksGrid.ReadOnly = true;
            this.TracksGrid.Size = new System.Drawing.Size(887, 124);
            this.TracksGrid.TabIndex = 8;
            this.TracksGrid.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.TracksGrid_RowEnter);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lblNoDataT4);
            this.groupBox3.Controls.Add(this.trkAccuracyGrid);
            this.groupBox3.Location = new System.Drawing.Point(8, 437);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(912, 144);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Accuracy Information";
            // 
            // lblNoDataT4
            // 
            this.lblNoDataT4.AutoSize = true;
            this.lblNoDataT4.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoDataT4.Location = new System.Drawing.Point(277, 59);
            this.lblNoDataT4.Name = "lblNoDataT4";
            this.lblNoDataT4.Size = new System.Drawing.Size(358, 26);
            this.lblNoDataT4.TabIndex = 14;
            this.lblNoDataT4.Text = "NO ACCURACY INFORMATION";
            // 
            // trkAccuracyGrid
            // 
            this.trkAccuracyGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.trkAccuracyGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.trkAccuracyGrid.Location = new System.Drawing.Point(13, 22);
            this.trkAccuracyGrid.Name = "trkAccuracyGrid";
            this.trkAccuracyGrid.ReadOnly = true;
            this.trkAccuracyGrid.Size = new System.Drawing.Size(887, 111);
            this.trkAccuracyGrid.TabIndex = 8;
            this.trkAccuracyGrid.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.trkAccuracyGrid_RowEnter);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblNoDataT3);
            this.groupBox2.Controls.Add(this.trkDescriptionGrid);
            this.groupBox2.Location = new System.Drawing.Point(8, 288);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(912, 144);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Description Information";
            // 
            // lblNoDataT3
            // 
            this.lblNoDataT3.AutoSize = true;
            this.lblNoDataT3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoDataT3.Location = new System.Drawing.Point(264, 59);
            this.lblNoDataT3.Name = "lblNoDataT3";
            this.lblNoDataT3.Size = new System.Drawing.Size(385, 26);
            this.lblNoDataT3.TabIndex = 12;
            this.lblNoDataT3.Text = "NO DESCRIPTION INFORMATION";
            // 
            // trkDescriptionGrid
            // 
            this.trkDescriptionGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.trkDescriptionGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.trkDescriptionGrid.Location = new System.Drawing.Point(13, 21);
            this.trkDescriptionGrid.Name = "trkDescriptionGrid";
            this.trkDescriptionGrid.ReadOnly = true;
            this.trkDescriptionGrid.Size = new System.Drawing.Size(887, 111);
            this.trkDescriptionGrid.TabIndex = 8;
            this.trkDescriptionGrid.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.trkDescriptionGrid_RowEnter);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblNoDataT1);
            this.groupBox1.Controls.Add(this.TrackInfoGrid);
            this.groupBox1.Location = new System.Drawing.Point(8, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(912, 110);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Track Information";
            // 
            // lblNoDataT1
            // 
            this.lblNoDataT1.AutoSize = true;
            this.lblNoDataT1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoDataT1.Location = new System.Drawing.Point(355, 42);
            this.lblNoDataT1.Name = "lblNoDataT1";
            this.lblNoDataT1.Size = new System.Drawing.Size(202, 26);
            this.lblNoDataT1.TabIndex = 13;
            this.lblNoDataT1.Text = "NO TRACK DATA";
            // 
            // TrackInfoGrid
            // 
            this.TrackInfoGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.TrackInfoGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TrackInfoGrid.Location = new System.Drawing.Point(13, 20);
            this.TrackInfoGrid.Name = "TrackInfoGrid";
            this.TrackInfoGrid.ReadOnly = true;
            this.TrackInfoGrid.Size = new System.Drawing.Size(887, 77);
            this.TrackInfoGrid.TabIndex = 7;
            this.TrackInfoGrid.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.TrackInfoGrid_RowEnter);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.lblNoDataW3);
            this.groupBox8.Controls.Add(this.wptAccuracyGrid);
            this.groupBox8.Location = new System.Drawing.Point(6, 395);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(912, 175);
            this.groupBox8.TabIndex = 22;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Accuracy Information";
            // 
            // lblNoDataW3
            // 
            this.lblNoDataW3.AutoSize = true;
            this.lblNoDataW3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoDataW3.Location = new System.Drawing.Point(277, 74);
            this.lblNoDataW3.Name = "lblNoDataW3";
            this.lblNoDataW3.Size = new System.Drawing.Size(358, 26);
            this.lblNoDataW3.TabIndex = 12;
            this.lblNoDataW3.Text = "NO ACCURACY INFORMATION";
            // 
            // wptAccuracyGrid
            // 
            this.wptAccuracyGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.wptAccuracyGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.wptAccuracyGrid.Location = new System.Drawing.Point(13, 20);
            this.wptAccuracyGrid.Name = "wptAccuracyGrid";
            this.wptAccuracyGrid.ReadOnly = true;
            this.wptAccuracyGrid.Size = new System.Drawing.Size(887, 142);
            this.wptAccuracyGrid.TabIndex = 7;
            this.wptAccuracyGrid.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.wptAccuracyGrid_RowEnter);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.lblNoDataW2);
            this.groupBox9.Controls.Add(this.wptDescriptionGrid);
            this.groupBox9.Location = new System.Drawing.Point(6, 208);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(912, 175);
            this.groupBox9.TabIndex = 21;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Description Information";
            // 
            // lblNoDataW2
            // 
            this.lblNoDataW2.AutoSize = true;
            this.lblNoDataW2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoDataW2.Location = new System.Drawing.Point(264, 74);
            this.lblNoDataW2.Name = "lblNoDataW2";
            this.lblNoDataW2.Size = new System.Drawing.Size(385, 26);
            this.lblNoDataW2.TabIndex = 11;
            this.lblNoDataW2.Text = "NO DESCRIPTION INFORMATION";
            // 
            // wptDescriptionGrid
            // 
            this.wptDescriptionGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.wptDescriptionGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.wptDescriptionGrid.Location = new System.Drawing.Point(13, 20);
            this.wptDescriptionGrid.Name = "wptDescriptionGrid";
            this.wptDescriptionGrid.ReadOnly = true;
            this.wptDescriptionGrid.Size = new System.Drawing.Size(887, 142);
            this.wptDescriptionGrid.TabIndex = 7;
            this.wptDescriptionGrid.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.wptDescriptionGrid_RowEnter);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.lblNoDataW1);
            this.groupBox10.Controls.Add(this.WaypointsGrid);
            this.groupBox10.Location = new System.Drawing.Point(6, 20);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(912, 175);
            this.groupBox10.TabIndex = 20;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Waypoints Required and Position Information";
            // 
            // lblNoDataW1
            // 
            this.lblNoDataW1.AutoSize = true;
            this.lblNoDataW1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoDataW1.Location = new System.Drawing.Point(332, 74);
            this.lblNoDataW1.Name = "lblNoDataW1";
            this.lblNoDataW1.Size = new System.Drawing.Size(249, 26);
            this.lblNoDataW1.TabIndex = 10;
            this.lblNoDataW1.Text = "NO WAYPOINT DATA";
            // 
            // WaypointsGrid
            // 
            this.WaypointsGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.WaypointsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.WaypointsGrid.Location = new System.Drawing.Point(13, 20);
            this.WaypointsGrid.Name = "WaypointsGrid";
            this.WaypointsGrid.ReadOnly = true;
            this.WaypointsGrid.Size = new System.Drawing.Size(887, 142);
            this.WaypointsGrid.TabIndex = 7;
            this.WaypointsGrid.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.WaypointsGrid_RowEnter);
            // 
            // lblNoDataT2
            // 
            this.lblNoDataT2.AutoSize = true;
            this.lblNoDataT2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoDataT2.Location = new System.Drawing.Point(269, 67);
            this.lblNoDataT2.Name = "lblNoDataT2";
            this.lblNoDataT2.Size = new System.Drawing.Size(374, 26);
            this.lblNoDataT2.TabIndex = 14;
            this.lblNoDataT2.Text = "NO TRACKPOINT INFORMATION";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lblNoDataR2);
            this.groupBox4.Controls.Add(this.RoutesGrid);
            this.groupBox4.Location = new System.Drawing.Point(5, 121);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(912, 161);
            this.groupBox4.TabIndex = 19;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Routepoints Required and Position Information";
            // 
            // lblNoDataR2
            // 
            this.lblNoDataR2.AutoSize = true;
            this.lblNoDataR2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoDataR2.Location = new System.Drawing.Point(269, 67);
            this.lblNoDataR2.Name = "lblNoDataR2";
            this.lblNoDataR2.Size = new System.Drawing.Size(376, 26);
            this.lblNoDataR2.TabIndex = 14;
            this.lblNoDataR2.Text = "NO ROUTEPOINT INFORMATION";
            // 
            // RoutesGrid
            // 
            this.RoutesGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.RoutesGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.RoutesGrid.Location = new System.Drawing.Point(13, 21);
            this.RoutesGrid.Name = "RoutesGrid";
            this.RoutesGrid.ReadOnly = true;
            this.RoutesGrid.Size = new System.Drawing.Size(887, 124);
            this.RoutesGrid.TabIndex = 8;
            this.RoutesGrid.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.RoutesGrid_RowEnter);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.lblNoDataR4);
            this.groupBox5.Controls.Add(this.rteAccuracyGrid);
            this.groupBox5.Location = new System.Drawing.Point(8, 437);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(912, 144);
            this.groupBox5.TabIndex = 18;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Accuracy Information";
            // 
            // lblNoDataR4
            // 
            this.lblNoDataR4.AutoSize = true;
            this.lblNoDataR4.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoDataR4.Location = new System.Drawing.Point(277, 59);
            this.lblNoDataR4.Name = "lblNoDataR4";
            this.lblNoDataR4.Size = new System.Drawing.Size(358, 26);
            this.lblNoDataR4.TabIndex = 14;
            this.lblNoDataR4.Text = "NO ACCURACY INFORMATION";
            // 
            // rteAccuracyGrid
            // 
            this.rteAccuracyGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.rteAccuracyGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.rteAccuracyGrid.Location = new System.Drawing.Point(13, 22);
            this.rteAccuracyGrid.Name = "rteAccuracyGrid";
            this.rteAccuracyGrid.ReadOnly = true;
            this.rteAccuracyGrid.Size = new System.Drawing.Size(887, 111);
            this.rteAccuracyGrid.TabIndex = 8;
            this.rteAccuracyGrid.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.rteAccuracyGrid_RowEnter);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.lblNoDataR3);
            this.groupBox6.Controls.Add(this.rteDescriptionGrid);
            this.groupBox6.Location = new System.Drawing.Point(8, 288);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(912, 144);
            this.groupBox6.TabIndex = 17;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Description Information";
            // 
            // lblNoDataR3
            // 
            this.lblNoDataR3.AutoSize = true;
            this.lblNoDataR3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoDataR3.Location = new System.Drawing.Point(264, 59);
            this.lblNoDataR3.Name = "lblNoDataR3";
            this.lblNoDataR3.Size = new System.Drawing.Size(385, 26);
            this.lblNoDataR3.TabIndex = 12;
            this.lblNoDataR3.Text = "NO DESCRIPTION INFORMATION";
            // 
            // rteDescriptionGrid
            // 
            this.rteDescriptionGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.rteDescriptionGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.rteDescriptionGrid.Location = new System.Drawing.Point(13, 21);
            this.rteDescriptionGrid.Name = "rteDescriptionGrid";
            this.rteDescriptionGrid.ReadOnly = true;
            this.rteDescriptionGrid.Size = new System.Drawing.Size(887, 111);
            this.rteDescriptionGrid.TabIndex = 8;
            this.rteDescriptionGrid.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.rteDescriptionGrid_RowEnter);
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.lblNoDataR1);
            this.groupBox11.Controls.Add(this.RouteInfoGrid);
            this.groupBox11.Location = new System.Drawing.Point(8, 9);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(912, 110);
            this.groupBox11.TabIndex = 16;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Route Information";
            // 
            // lblNoDataR1
            // 
            this.lblNoDataR1.AutoSize = true;
            this.lblNoDataR1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoDataR1.Location = new System.Drawing.Point(355, 42);
            this.lblNoDataR1.Name = "lblNoDataR1";
            this.lblNoDataR1.Size = new System.Drawing.Size(204, 26);
            this.lblNoDataR1.TabIndex = 13;
            this.lblNoDataR1.Text = "NO ROUTE DATA";
            // 
            // RouteInfoGrid
            // 
            this.RouteInfoGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.RouteInfoGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.RouteInfoGrid.Location = new System.Drawing.Point(13, 20);
            this.RouteInfoGrid.Name = "RouteInfoGrid";
            this.RouteInfoGrid.ReadOnly = true;
            this.RouteInfoGrid.Size = new System.Drawing.Size(887, 77);
            this.RouteInfoGrid.TabIndex = 7;
            this.RouteInfoGrid.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.RouteInfoGrid_RowEnter);
            // 
            // txtVersion
            // 
            this.txtVersion.Enabled = false;
            this.txtVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVersion.Location = new System.Drawing.Point(88, 18);
            this.txtVersion.Name = "txtVersion";
            this.txtVersion.ReadOnly = true;
            this.txtVersion.Size = new System.Drawing.Size(355, 23);
            this.txtVersion.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(19, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Version:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(481, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "Creator:";
            // 
            // txtCreator
            // 
            this.txtCreator.Enabled = false;
            this.txtCreator.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCreator.Location = new System.Drawing.Point(550, 18);
            this.txtCreator.Name = "txtCreator";
            this.txtCreator.ReadOnly = true;
            this.txtCreator.Size = new System.Drawing.Size(359, 23);
            this.txtCreator.TabIndex = 6;
            // 
            // txtAuthor
            // 
            this.txtAuthor.Enabled = false;
            this.txtAuthor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAuthor.Location = new System.Drawing.Point(550, 58);
            this.txtAuthor.Name = "txtAuthor";
            this.txtAuthor.ReadOnly = true;
            this.txtAuthor.Size = new System.Drawing.Size(359, 23);
            this.txtAuthor.TabIndex = 9;
            // 
            // txtName
            // 
            this.txtName.Enabled = false;
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.Location = new System.Drawing.Point(88, 58);
            this.txtName.Name = "txtName";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(355, 23);
            this.txtName.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(487, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 17);
            this.label5.TabIndex = 7;
            this.label5.Text = "Author:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(33, 61);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 17);
            this.label6.TabIndex = 7;
            this.label6.Text = "Name:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(43, 105);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 17);
            this.label7.TabIndex = 7;
            this.label7.Text = "URL:";
            // 
            // txtUrl
            // 
            this.txtUrl.Enabled = false;
            this.txtUrl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUrl.Location = new System.Drawing.Point(88, 102);
            this.txtUrl.Name = "txtUrl";
            this.txtUrl.ReadOnly = true;
            this.txtUrl.Size = new System.Drawing.Size(821, 23);
            this.txtUrl.TabIndex = 8;
            // 
            // txtUrlName
            // 
            this.txtUrlName.Enabled = false;
            this.txtUrlName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUrlName.Location = new System.Drawing.Point(108, 147);
            this.txtUrlName.Name = "txtUrlName";
            this.txtUrlName.ReadOnly = true;
            this.txtUrlName.Size = new System.Drawing.Size(801, 23);
            this.txtUrlName.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(19, 150);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(90, 17);
            this.label8.TabIndex = 10;
            this.label8.Text = "URL Name:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(19, 197);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(113, 17);
            this.label9.TabIndex = 5;
            this.label9.Text = "Creation Date:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(467, 197);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 17);
            this.label10.TabIndex = 5;
            this.label10.Text = "Email:";
            // 
            // txtDate
            // 
            this.txtDate.Enabled = false;
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.Location = new System.Drawing.Point(132, 194);
            this.txtDate.Name = "txtDate";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(311, 23);
            this.txtDate.TabIndex = 6;
            // 
            // txtEmail
            // 
            this.txtEmail.Enabled = false;
            this.txtEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.Location = new System.Drawing.Point(518, 194);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.ReadOnly = true;
            this.txtEmail.Size = new System.Drawing.Size(391, 23);
            this.txtEmail.TabIndex = 6;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.rtxDescription);
            this.groupBox12.Location = new System.Drawing.Point(22, 237);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(887, 120);
            this.groupBox12.TabIndex = 12;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "GPX File Description";
            // 
            // rtxDescription
            // 
            this.rtxDescription.Location = new System.Drawing.Point(10, 19);
            this.rtxDescription.Name = "rtxDescription";
            this.rtxDescription.ReadOnly = true;
            this.rtxDescription.Size = new System.Drawing.Size(867, 91);
            this.rtxDescription.TabIndex = 1;
            this.rtxDescription.Text = "";
            // 
            // txtBounds
            // 
            this.txtBounds.Enabled = false;
            this.txtBounds.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBounds.Location = new System.Drawing.Point(532, 558);
            this.txtBounds.Name = "txtBounds";
            this.txtBounds.ReadOnly = true;
            this.txtBounds.Size = new System.Drawing.Size(377, 23);
            this.txtBounds.TabIndex = 15;
            // 
            // txtKeywords
            // 
            this.txtKeywords.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtKeywords.Enabled = false;
            this.txtKeywords.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKeywords.Location = new System.Drawing.Point(102, 558);
            this.txtKeywords.Name = "txtKeywords";
            this.txtKeywords.ReadOnly = true;
            this.txtKeywords.Size = new System.Drawing.Size(341, 23);
            this.txtKeywords.TabIndex = 16;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(467, 561);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 17);
            this.label11.TabIndex = 13;
            this.label11.Text = "Bounds:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(19, 561);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(82, 17);
            this.label12.TabIndex = 14;
            this.label12.Text = "Keywords:";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.rtxRaw);
            this.groupBox13.Location = new System.Drawing.Point(22, 363);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(886, 185);
            this.groupBox13.TabIndex = 17;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "GPX Raw Data";
            // 
            // rtxRaw
            // 
            this.rtxRaw.Location = new System.Drawing.Point(10, 18);
            this.rtxRaw.Name = "rtxRaw";
            this.rtxRaw.ReadOnly = true;
            this.rtxRaw.Size = new System.Drawing.Size(867, 158);
            this.rtxRaw.TabIndex = 5;
            this.rtxRaw.Text = "";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(956, 733);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.Namespace);
            this.Controls.Add(this.filePath);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GPX READER";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TracksGrid)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trkAccuracyGrid)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trkDescriptionGrid)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrackInfoGrid)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.wptAccuracyGrid)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.wptDescriptionGrid)).EndInit();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WaypointsGrid)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RoutesGrid)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rteAccuracyGrid)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rteDescriptionGrid)).EndInit();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RouteInfoGrid)).EndInit();
            this.groupBox12.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox filePath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Namespace;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.DataGridView TracksGrid;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lblNoDataT4;
        private System.Windows.Forms.DataGridView trkAccuracyGrid;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblNoDataT3;
        private System.Windows.Forms.DataGridView trkDescriptionGrid;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblNoDataT1;
        private System.Windows.Forms.DataGridView TrackInfoGrid;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label lblNoDataW3;
        private System.Windows.Forms.DataGridView wptAccuracyGrid;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label lblNoDataW2;
        private System.Windows.Forms.DataGridView wptDescriptionGrid;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label lblNoDataW1;
        private System.Windows.Forms.DataGridView WaypointsGrid;
        private System.Windows.Forms.Label lblNoDataT2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label lblNoDataR2;
        private System.Windows.Forms.DataGridView RoutesGrid;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label lblNoDataR4;
        private System.Windows.Forms.DataGridView rteAccuracyGrid;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label lblNoDataR3;
        private System.Windows.Forms.DataGridView rteDescriptionGrid;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label lblNoDataR1;
        private System.Windows.Forms.DataGridView RouteInfoGrid;
        private System.Windows.Forms.TextBox txtVersion;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCreator;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtAuthor;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtUrlName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtUrl;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.RichTextBox rtxDescription;
        private System.Windows.Forms.TextBox txtBounds;
        private System.Windows.Forms.TextBox txtKeywords;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.RichTextBox rtxRaw;
    }
}

