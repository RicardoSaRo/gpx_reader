﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPX_Reader
{
    public class OpAccuracy
    {
        public string Name { get; set; }

        public string Fix { get; set; } //<fix>

        public string Satellites { get; set; } //<sat>

        public string HDOP { get; set; } //<hdop>

        public string VDOP { get; set; } //<vdop>

        public string PDOP { get; set; } //<pdop>

        public string AgeOfDGPSData { get; set; } //<ageofdgpsdata>

        public string DGPSid { get; set; } //<dgpsid>
    }
}
