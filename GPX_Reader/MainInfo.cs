﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPX_Reader
{
    public class MainInfo
    {
        public string Main_Name { get; set; } //--> Track or Route name

        public string Name { get; set; } //<name> //--> Trackpoint or Routepoint name

        public string Latitude { get; set; } //<lat>

        public string Longitude { get; set; } //<lon>

        public string Elevation { get; set; } //<ele>

        public string Time { get; set; } //<time>

        public string Magvar { get; set; } //<magvar>

        public string GeoIdHeight { get; set; } //<geoidheight>
    }
}
