﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace GPX_Reader
{
    public class GPX
    {
        public XDocument gpxDoc { get; set; }

        public XNamespace NSpace { get; set; }

        public string gpxRaw { get; set; }

        public string gpxFilePath { get; set; }

        public GPXinfo gpxMainInfo { get; set; }

        public List<MainInfo> WaypointsList { get; set; }

        public List<OpDescription> WDescription { get; set; }

        public List<OpAccuracy> WAccuracy { get; set; }

        public List<DetailedInfo> Track { get; set; }

        public List<MainInfo> TrackpointsList { get; set; }

        public List<OpDescription> TDescription { get; set; }

        public List<OpAccuracy> TAccuracy { get; set; }

        public List<DetailedInfo> Route { get; set; }

        public List<MainInfo> RoutepointsList { get; set; }

        public List<OpDescription> RDescription { get; set; }

        public List<OpAccuracy> RAccuracy { get; set; }
    }
}
