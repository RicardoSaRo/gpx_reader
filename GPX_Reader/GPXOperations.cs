﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace GPX_Reader
{
    public class GPXOperations
    {
        public static XNamespace GetGpxNameSpace()
        {
            XNamespace gpx = XNamespace.Get("http://www.topografix.com/GPX/1/1");
            return gpx;
        }

        public static void FillGPXSource(GPX GpxSource)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                //openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "gpx files (*.gpx)|*.gpx|All files(*.*)|*.*";
                openFileDialog.FilterIndex = 1;
                openFileDialog.RestoreDirectory = true;

                //--> Gets the Namespace
                GpxSource.NSpace = GetGpxNameSpace();

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //--> Get the path of specified file
                    GpxSource.gpxFilePath = openFileDialog.FileName;

                    //--> Read the contents of the file into a stream to pass info as an XML
                    XDocument doc = new XDocument();
                    var fileStream = openFileDialog.OpenFile();
                    GpxSource.gpxDoc = XDocument.Load(fileStream);
                    //--> Read the contents of the file into a stream to pass info as a string to check Raw content
                    fileStream = openFileDialog.OpenFile();
                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        GpxSource.gpxRaw = reader.ReadToEnd();
                    }

                    GpxSource.gpxMainInfo = LoadGPXInfo(GpxSource);

                    GpxSource.WaypointsList = LoadGPXMainInfo(GpxSource);
                    GpxSource.WDescription = LoadGPXDescription(GpxSource, "wpt");
                    GpxSource.WAccuracy = LoadGPXAccuracy(GpxSource, "wpt");

                    GpxSource.Track = LoadGPXDetailedInfo(GpxSource, "trk");
                    GpxSource.TrackpointsList = LoadGPXMainInfo(GpxSource,"trk","trkpt");
                    GpxSource.TDescription = LoadGPXDescription(GpxSource, "trk");
                    GpxSource.TAccuracy = LoadGPXAccuracy(GpxSource, "trk");

                    GpxSource.Route = LoadGPXDetailedInfo(GpxSource, "rte");
                    GpxSource.RoutepointsList = LoadGPXMainInfo(GpxSource, "rte", "rtept");
                    GpxSource.RDescription = LoadGPXDescription(GpxSource, "rte");
                    GpxSource.RAccuracy = LoadGPXAccuracy(GpxSource, "rte");
                }
            }
        }

        public static bool[] CheckListMainElements(List<MainInfo> maininfo)
        {
            bool[] cols = new bool[8];
            foreach (var item in maininfo)
            {
                if (cols[0] != true) cols[0] = (item.Main_Name != null);
                if (cols[1] != true) cols[1] = (item.Name != null);
                if (cols[2] != true) cols[2] = (item.Latitude != null);
                if (cols[3] != true) cols[3] = (item.Longitude != null);
                if (cols[4] != true) cols[4] = (item.Elevation != null);
                if (cols[5] != true) cols[5] = (item.Time != null);
                if (cols[6] != true) cols[6] = (item.Magvar != null);
                if (cols[7] != true) cols[7] = (item.GeoIdHeight != null);

            }
            return cols;
        }

        public static bool[] CheckListDescriptionElements(List<OpDescription> desc)
        {
            bool[] cols = new bool[8];
            foreach (var item in desc)
            {
                if (cols[0] != true) cols[0] = (item.Name != null);
                if (cols[1] != true) cols[1] = (item.Comment != null);
                if (cols[2] != true) cols[2] = (item.Description != null);
                if (cols[3] != true) cols[3] = (item.Source != null);
                if (cols[4] != true) cols[4] = (item.Url != null);
                if (cols[5] != true) cols[5] = (item.UrlName != null);
                if (cols[6] != true) cols[6] = (item.Symbol != null);
                if (cols[7] != true) cols[7] = (item.Type != null);
            }
            if (cols[1] == false && cols[2] == false && cols[3] == false &&
                cols[4] == false && cols[5] == false && cols[6] == false && cols[7] == false)
            {
                cols[0] = false;
            }
            return cols;
        }

        public static bool[] CheckListDetailedElements(List<DetailedInfo> dtlinfoList)
        {
            bool[] cols = new bool[7];
            foreach (var item in dtlinfoList)
            {
                if (cols[0] != true) cols[0] = (item.Name != null);
                if (cols[1] != true) cols[1] = (item.Comment != null);
                if (cols[2] != true) cols[2] = (item.Description != null);
                if (cols[3] != true) cols[3] = (item.Source != null);
                if (cols[4] != true) cols[4] = (item.Url != null);
                if (cols[5] != true) cols[5] = (item.UrlName != null);
                if (cols[6] != true) cols[6] = (item.Number != null);
            }
            return cols;
        }

        internal static bool[] CheckListAccuracyElements(List<OpAccuracy> acc)
        {
            bool[] cols = new bool[8];
            foreach (var item in acc)
            {
                if (cols[0] != true) cols[0] = (item.Name != null);
                if (cols[1] != true) cols[1] = (item.Fix != null);
                if (cols[2] != true) cols[2] = (item.Satellites != null);
                if (cols[3] != true) cols[3] = (item.HDOP != null);
                if (cols[4] != true) cols[4] = (item.VDOP != null);
                if (cols[5] != true) cols[5] = (item.PDOP != null);
                if (cols[6] != true) cols[6] = (item.AgeOfDGPSData != null);
                if (cols[7] != true) cols[7] = (item.DGPSid != null);
            }
            if (cols[1] == false && cols[2] == false && cols[3] == false &&
                cols[4] == false && cols[5] == false && cols[6] == false && cols[7] == false)
            {
                cols[0] = false;
            }
            return cols;
        }

        public static List<MainInfo> LoadGPXMainInfo(GPX GpxSource) //--> Made to Load Waypoints Information
        {
            var waypoints = from waypoint in GpxSource.gpxDoc.Descendants(GpxSource.NSpace + "wpt")
                            select new
                            {
                                Name = waypoint.Element(GpxSource.NSpace + "name") != null ? waypoint.Element(GpxSource.NSpace + "name").Value : null,
                                Latitude = waypoint.Attribute("lat").Value,
                                Longitude = waypoint.Attribute("lon").Value,
                                Elevation = waypoint.Element(GpxSource.NSpace + "ele") != null ? waypoint.Element(GpxSource.NSpace + "ele").Value : null,
                                Time = waypoint.Element(GpxSource.NSpace + "cmt") != null ? waypoint.Element(GpxSource.NSpace + "cmt").Value : null,
                                Magvar = waypoint.Element(GpxSource.NSpace + "magvar") != null ? waypoint.Element(GpxSource.NSpace + "magvar").Value : null,
                                GeoIdHeight = waypoint.Element(GpxSource.NSpace + "geoidheight") != null ? waypoint.Element(GpxSource.NSpace + "geoidheight").Value : null
                            };

            List<MainInfo> wypntsList = new List<MainInfo>();
            foreach (var wpt in waypoints)
            {
                MainInfo wypnts = new MainInfo();
                wypnts.Main_Name = null;
                wypnts.Name = wpt.Name;
                wypnts.Latitude = wpt.Latitude;
                wypnts.Longitude = wpt.Longitude;
                wypnts.Elevation = wpt.Elevation;
                wypnts.Time = wpt.Time;
                wypnts.Magvar = wpt.Magvar;
                wypnts.GeoIdHeight = wpt.GeoIdHeight;
                wypntsList.Add(wypnts);
            }

            return wypntsList;
        }

        public static List<MainInfo> LoadGPXMainInfo(GPX GpxSource, string infoType, string segType) //--> Load Trackpoints and Routepoints Information
        {
            GpxSource.NSpace = GetGpxNameSpace();
            var MainInfos = from info in GpxSource.gpxDoc.Descendants(GpxSource.NSpace + infoType)
                         select new
                         {
                             Main_Name = info.Element(GpxSource.NSpace + "name") != null ? info.Element(GpxSource.NSpace + "name").Value : null,
                             Segs = (
                                from infoPoint in info.Descendants(GpxSource.NSpace + segType)
                                select new
                                {
                                    Name = infoPoint.Element(GpxSource.NSpace + "name") != null ? infoPoint.Element(GpxSource.NSpace + "name").Value : null,
                                    Latitude = infoPoint.Attribute("lat").Value,
                                    Longitude = infoPoint.Attribute("lon").Value,
                                    Elevation = infoPoint.Element(GpxSource.NSpace + "ele") != null ? infoPoint.Element(GpxSource.NSpace + "ele").Value : null,
                                    Time = infoPoint.Element(GpxSource.NSpace + "time") != null ? infoPoint.Element(GpxSource.NSpace + "time").Value : null,
                                    Magvar = infoPoint.Element(GpxSource.NSpace + "magvar") != null ? infoPoint.Element(GpxSource.NSpace + "magvar").Value : null,
                                    GeoIdHeight = infoPoint.Element(GpxSource.NSpace + "geoidheight") != null ? infoPoint.Element(GpxSource.NSpace + "geoidheight").Value : null
                                }
                              )
                         };

            List<MainInfo> maininfoList = new List<MainInfo>();
            foreach (var minfo in MainInfos)
            {
                foreach (var infoSeg in minfo.Segs)
                {
                    MainInfo infos = new MainInfo();
                    infos.Main_Name = minfo.Main_Name;
                    infos.Name = infoSeg.Name;
                    infos.Latitude = infoSeg.Latitude;
                    infos.Longitude = infoSeg.Longitude;
                    infos.Elevation = infoSeg.Elevation;
                    infos.Time = infoSeg.Time;
                    infos.Magvar = infoSeg.Magvar;
                    infos.GeoIdHeight = infoSeg.GeoIdHeight;
                    maininfoList.Add(infos);
                }
            }
            return maininfoList;
        }
        
        public static GPXinfo LoadGPXInfo(GPX GpxSource)
        {
            GpxSource.NSpace = GetGpxNameSpace();
            var gpxinfo = from info in GpxSource.gpxDoc.Descendants(GpxSource.NSpace + "gpx")
                            select new
                            {
                                Version = info.Attribute("version").Value,
                                Creator = info.Attribute("creator").Value,
                                Name = info.Element(GpxSource.NSpace + "name") != null ? info.Element(GpxSource.NSpace + "name").Value : null,
                                Description = info.Element(GpxSource.NSpace + "desc") != null ? info.Element(GpxSource.NSpace + "desc").Value : null,
                                Author = info.Element(GpxSource.NSpace + "author") != null ? info.Element(GpxSource.NSpace + "author").Value : null,
                                Email = info.Element(GpxSource.NSpace + "email") != null ? info.Element(GpxSource.NSpace + "email").Value : null,
                                Url = info.Element(GpxSource.NSpace + "url") != null ? info.Element(GpxSource.NSpace + "url").Value : null,
                                UrlName = info.Element(GpxSource.NSpace + "urlname") != null ? info.Element(GpxSource.NSpace + "urlname").Value : null,
                                Time = info.Element(GpxSource.NSpace + "time") != null ? info.Element(GpxSource.NSpace + "time").Value : null,
                                Keywords = info.Element(GpxSource.NSpace + "keywords") != null ? info.Element(GpxSource.NSpace + "keywords").Value : null,
                                Bounds = info.Element(GpxSource.NSpace + "bounds") != null ? info.Element(GpxSource.NSpace + "bounds").Value : null
                            };
            
            var gpxs = gpxinfo.FirstOrDefault();

            GPXinfo gpx = new GPXinfo();
            gpx.Version = gpxs.Version;
            gpx.Creator = gpxs.Creator;
            gpx.Name = gpxs.Name;
            gpx.Description = gpxs.Description;
            gpx.Author = gpxs.Author;
            gpx.Email = gpxs.Email;
            gpx.Url = gpxs.Url;
            gpx.UrlName = gpxs.UrlName;
            gpx.Time = gpxs.Time;
            gpx.Keywords = gpxs.Keywords;
            gpx.Bounds = gpxs.Bounds;

            return gpx;
        }

        public static List<DetailedInfo> LoadGPXDetailedInfo(GPX GpxSource, string infoType)
        {
            GpxSource.NSpace = GetGpxNameSpace();
            var infos = from info in GpxSource.gpxDoc.Descendants(GpxSource.NSpace + infoType)
                          select new
                          {
                              Name = info.Element(GpxSource.NSpace + "name") != null ? info.Element(GpxSource.NSpace + "name").Value : null,
                              Comment = info.Element(GpxSource.NSpace + "cmt") != null ? info.Element(GpxSource.NSpace + "cmt").Value : null,
                              Description = info.Element(GpxSource.NSpace + "desc") != null ? info.Element(GpxSource.NSpace + "desc").Value : null,
                              Source = info.Element(GpxSource.NSpace + "src") != null ? info.Element(GpxSource.NSpace + "src").Value : null,
                              Url = info.Element(GpxSource.NSpace + "url") != null ? info.Element(GpxSource.NSpace + "url").Value : null,
                              UrlName = info.Element(GpxSource.NSpace + "urlname") != null ? info.Element(GpxSource.NSpace + "urlname").Value : null,
                              Number = info.Element(GpxSource.NSpace + "number") != null ? info.Element(GpxSource.NSpace + "number").Value : null
                          };
            
            List<DetailedInfo> list = new List<DetailedInfo>();
            foreach (var infox in infos)
            {
                DetailedInfo inf = new DetailedInfo();
                inf.Name = infox.Name;
                inf.Comment = infox.Comment;
                inf.Description = infox.Description;
                inf.Source = infox.Source;
                inf.Url = infox.Url;
                inf.UrlName = infox.UrlName;
                inf.Number = infox.Number;
                list.Add(inf);
            }
            
            return list;
        }

        public static List<OpDescription> LoadGPXDescription(GPX GpxSource, string infoType)
        {
            GpxSource.NSpace = GetGpxNameSpace();
            var descript = from desc in GpxSource.gpxDoc.Descendants(GpxSource.NSpace + infoType)
                        select new
                        {
                            Name = desc.Element(GpxSource.NSpace + "name") != null ? desc.Element(GpxSource.NSpace + "name").Value : null,
                            Comment = desc.Element(GpxSource.NSpace + "cmt") != null ? desc.Element(GpxSource.NSpace + "cmt").Value : null,
                            Description = desc.Element(GpxSource.NSpace + "desc") != null ? desc.Element(GpxSource.NSpace + "desc").Value : null,
                            Source = desc.Element(GpxSource.NSpace + "src") != null ? desc.Element(GpxSource.NSpace + "src").Value : null,
                            Url = desc.Element(GpxSource.NSpace + "url") != null ? desc.Element(GpxSource.NSpace + "url").Value : null,
                            UrlName = desc.Element(GpxSource.NSpace + "urlname") != null ? desc.Element(GpxSource.NSpace + "urlname").Value : null,
                            Symbol = desc.Element(GpxSource.NSpace + "sym") != null ? desc.Element(GpxSource.NSpace + "sym").Value : null,
                            Type = desc.Element(GpxSource.NSpace + "type") != null ? desc.Element(GpxSource.NSpace + "type").Value : null
                        };

            List<OpDescription> list = new List<OpDescription>();
            foreach (var item in descript)
            {
                OpDescription dsc = new OpDescription();
                dsc.Name = item.Name;
                dsc.Comment = item.Comment;
                dsc.Description = item.Description;
                dsc.Source = item.Source;
                dsc.Url = item.Url;
                dsc.UrlName = item.UrlName;
                dsc.Symbol = item.Symbol;
                dsc.Type = item.Type;
                list.Add(dsc);
            }

            return list;
        }

        public static List<OpAccuracy> LoadGPXAccuracy(GPX GpxSource, string infoType)
        {
            GpxSource.NSpace = GetGpxNameSpace();
            var Accuracy = from accur in GpxSource.gpxDoc.Descendants(GpxSource.NSpace + infoType)
                           select new
                           {
                               Name = accur.Element(GpxSource.NSpace + "name") != null ? accur.Element(GpxSource.NSpace + "name").Value : null,
                               Fix = accur.Element(GpxSource.NSpace + "fix") != null ? accur.Element(GpxSource.NSpace + "fix").Value : null,
                               Satellites = accur.Element(GpxSource.NSpace + "sat") != null ? accur.Element(GpxSource.NSpace + "sat").Value : null,
                               HDOP = accur.Element(GpxSource.NSpace + "hdop") != null ? accur.Element(GpxSource.NSpace + "hdop").Value : null,
                               VDOP = accur.Element(GpxSource.NSpace + "vdop") != null ? accur.Element(GpxSource.NSpace + "vdop").Value : null,
                               PDOP = accur.Element(GpxSource.NSpace + "pdop") != null ? accur.Element(GpxSource.NSpace + "pdop").Value : null,
                               AgeOfDGPSdata = accur.Element(GpxSource.NSpace + "ageofdgpsdata") != null ? accur.Element(GpxSource.NSpace + "ageofdgpsdata").Value : null,
                               DGPSid = accur.Element(GpxSource.NSpace + "dgpsid") != null ? accur.Element(GpxSource.NSpace + "dgpsid").Value : null
                           };

            List<OpAccuracy> list = new List<OpAccuracy>();
            foreach (var item in Accuracy)
            {
                OpAccuracy acc = new OpAccuracy();
                acc.Name = item.Name;
                acc.Fix = item.Fix;
                acc.Satellites = item.Satellites;
                acc.HDOP = item.HDOP;
                acc.VDOP = item.VDOP;
                acc.PDOP = item.PDOP;
                acc.AgeOfDGPSData = item.AgeOfDGPSdata;
                acc.DGPSid = item.DGPSid;
                list.Add(acc);
            }

            return list;
        }
    }
}
