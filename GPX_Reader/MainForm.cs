﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml.Linq;
using System.Xml;
using GPX_Reader;

namespace GPX_Reader
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        public GPX GpxSource = new GPX();

        private void button1_Click(object sender, EventArgs e)
        {
            GPXOperations.FillGPXSource(GpxSource);

            //////--> Object info distribution <--//////
            //-->GPX Main Info
            filePath.Text = GpxSource.gpxFilePath;
            Namespace.Text = GpxSource.NSpace.ToString();
            txtVersion.Text = GpxSource.gpxMainInfo.Version;
            txtCreator.Text = GpxSource.gpxMainInfo.Creator;
            txtName.Text = GpxSource.gpxMainInfo.Name;
            txtAuthor.Text = GpxSource.gpxMainInfo.Author;
            txtUrl.Text = GpxSource.gpxMainInfo.Url;
            txtUrlName.Text = GpxSource.gpxMainInfo.UrlName;
            txtDate.Text = GpxSource.gpxMainInfo.Time;
            txtEmail.Text = GpxSource.gpxMainInfo.Email;
            rtxDescription.Text = GpxSource.gpxMainInfo.Description;
            rtxRaw.Text = GpxSource.gpxRaw;
            txtKeywords.Text = GpxSource.gpxMainInfo.Keywords;
            txtBounds.Text = GpxSource.gpxMainInfo.Bounds;
            //-->Waypoints
            WaypointsGrid.DataSource = GpxSource.WaypointsList;
            wptDescriptionGrid.DataSource = GpxSource.WDescription;
            wptAccuracyGrid.DataSource = GpxSource.WAccuracy;
            //-->Tracks
            TrackInfoGrid.DataSource = GpxSource.Track;
            TracksGrid.DataSource = GpxSource.TrackpointsList;
            trkDescriptionGrid.DataSource = GpxSource.TDescription;
            trkAccuracyGrid.DataSource = GpxSource.TAccuracy;
            //-->Routes
            RouteInfoGrid.DataSource = GpxSource.Route;
            RoutesGrid.DataSource = GpxSource.RoutepointsList;
            rteDescriptionGrid.DataSource = GpxSource.RDescription;
            rteAccuracyGrid.DataSource = GpxSource.RAccuracy;

            //--> No Data Labels
            lblNoDataR1.Visible = (GpxSource.RoutepointsList.Count > 0) ? false : true;
            lblNoDataR2.Visible = (GpxSource.RoutepointsList.Count > 0) ? false : true;
            lblNoDataR3.Visible = (GpxSource.RoutepointsList.Count > 0) ? false : true;
            lblNoDataR4.Visible = (GpxSource.RoutepointsList.Count > 0) ? false : true;
            lblNoDataT1.Visible = (GpxSource.TrackpointsList.Count > 0) ? false : true;
            lblNoDataT2.Visible = (GpxSource.TrackpointsList.Count > 0) ? false : true;
            lblNoDataT3.Visible = (GpxSource.TrackpointsList.Count > 0) ? false : true;
            lblNoDataT4.Visible = (GpxSource.TrackpointsList.Count > 0) ? false : true;
            lblNoDataW1.Visible = (GpxSource.WaypointsList.Count > 0) ? false : true;
            lblNoDataW2.Visible = (GpxSource.WaypointsList.Count > 0) ? false : true;
            lblNoDataW3.Visible = (GpxSource.WaypointsList.Count > 0) ? false : true;

        }

        private void MainForm_Load(object sender, EventArgs e)
        {

            
        }

        private void DescriptionGridFormat(DataGridView grid, List<OpDescription> desc)
        {
            bool[] nullCheck = GPXOperations.CheckListDescriptionElements(desc);
            grid.Columns[0].Visible = (nullCheck[0]);
            if (nullCheck[0]) grid.Columns[0].HeaderText = "Name";
            grid.Columns[1].Visible = (nullCheck[1]);
            if (nullCheck[1]) grid.Columns[1].HeaderText = "Comment";
            grid.Columns[2].Visible = (nullCheck[2]);
            if (nullCheck[2]) grid.Columns[2].HeaderText = "Description";
            grid.Columns[3].Visible = (nullCheck[3]);
            if (nullCheck[3]) grid.Columns[3].HeaderText = "Source";
            grid.Columns[4].Visible = (nullCheck[4]);
            if (nullCheck[4]) grid.Columns[4].HeaderText = "Url";
            grid.Columns[5].Visible = (nullCheck[5]);
            if (nullCheck[5]) grid.Columns[5].HeaderText = "Url Name";
            grid.Columns[6].Visible = (nullCheck[6]);
            if (nullCheck[6]) grid.Columns[6].HeaderText = "Symbol";
            grid.Columns[7].Visible = (nullCheck[7]);
            if (nullCheck[7]) grid.Columns[7].HeaderText = "Type";
        }
    

        void DetailedInfoGridFormat(DataGridView grid, List<DetailedInfo> dtlinfoList)
        {
            bool[] nullCheck = GPXOperations.CheckListDetailedElements(dtlinfoList);
            grid.Columns[0].Visible = (nullCheck[0]);
            if (nullCheck[0]) grid.Columns[0].HeaderText = "Name";
            grid.Columns[1].Visible = (nullCheck[1]);
            if (nullCheck[1]) grid.Columns[1].HeaderText = "Comment";
            grid.Columns[2].Visible = (nullCheck[2]);
            if (nullCheck[2]) grid.Columns[2].HeaderText = "Description";
            grid.Columns[3].Visible = (nullCheck[3]);
            if (nullCheck[3]) grid.Columns[3].HeaderText = "Source";
            grid.Columns[4].Visible = (nullCheck[4]);
            if (nullCheck[4]) grid.Columns[4].HeaderText = "Url";
            grid.Columns[5].Visible = (nullCheck[5]);
            if (nullCheck[5]) grid.Columns[5].HeaderText = "Url Name";
            grid.Columns[6].Visible = (nullCheck[6]);
            if (nullCheck[6]) grid.Columns[6].HeaderText = "Number";
        }

        void MainInfoGridFormat(DataGridView grid, List<MainInfo> maininfoList)
        {
            bool[] nullCheck = GPXOperations.CheckListMainElements(maininfoList);
            grid.Columns[0].Visible = (nullCheck[0]);
            if (nullCheck[0]) grid.Columns[0].HeaderText = "Main Name";
            grid.Columns[1].Visible = (nullCheck[1]);
            if (nullCheck[1]) grid.Columns[1].HeaderText = "Point Name";
            grid.Columns[2].Visible = (nullCheck[2]);
            if (nullCheck[2]) grid.Columns[2].HeaderText = "Latitude";
            grid.Columns[3].Visible = (nullCheck[3]);
            if (nullCheck[3]) grid.Columns[3].HeaderText = "Longitude";
            grid.Columns[4].Visible = (nullCheck[4]);
            if (nullCheck[4]) grid.Columns[4].HeaderText = "Elevation";
            grid.Columns[5].Visible = (nullCheck[5]);
            if (nullCheck[5]) grid.Columns[5].HeaderText = "Time";
            grid.Columns[6].Visible = (nullCheck[6]);
            if (nullCheck[6]) grid.Columns[6].HeaderText = "Mag.Var.";
            grid.Columns[7].Visible = (nullCheck[7]);
            if (nullCheck[7]) grid.Columns[7].HeaderText = "Geo Id Height";
        }


        private void AccuracyGridFormat(DataGridView grid, List<OpAccuracy> acc)
        {
            bool[] nullCheck = GPXOperations.CheckListAccuracyElements(acc);
            grid.Columns[0].Visible = (nullCheck[0]);
            if (nullCheck[0]) grid.Columns[0].HeaderText = "Main Name";
            grid.Columns[1].Visible = (nullCheck[1]);
            if (nullCheck[1]) grid.Columns[1].HeaderText = "Point Name";
            grid.Columns[2].Visible = (nullCheck[2]);
            if (nullCheck[2]) grid.Columns[2].HeaderText = "Latitude";
            grid.Columns[3].Visible = (nullCheck[3]);
            if (nullCheck[3]) grid.Columns[3].HeaderText = "Longitude";
            grid.Columns[4].Visible = (nullCheck[4]);
            if (nullCheck[4]) grid.Columns[4].HeaderText = "Elevation";
            grid.Columns[5].Visible = (nullCheck[5]);
            if (nullCheck[5]) grid.Columns[5].HeaderText = "Time";
            grid.Columns[6].Visible = (nullCheck[6]);
            if (nullCheck[6]) grid.Columns[6].HeaderText = "Mag.Var.";
            grid.Columns[7].Visible = (nullCheck[7]);
            if (nullCheck[7]) grid.Columns[7].HeaderText = "Geo Id Height";
        }

        private void WaypointsGrid_RowEnter(object sender, DataGridViewCellEventArgs grid)
        {
            MainInfoGridFormat(WaypointsGrid,GpxSource.WaypointsList);
        }

        private void TracksGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            MainInfoGridFormat(TracksGrid,GpxSource.TrackpointsList);
        }

        private void RoutesGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            MainInfoGridFormat(RoutesGrid, GpxSource.RoutepointsList);
        }

        private void TrackInfoGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            DetailedInfoGridFormat(TrackInfoGrid, GpxSource.Track);
        }

        private void RouteInfoGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            DetailedInfoGridFormat(RouteInfoGrid, GpxSource.Route);
        }

        private void wptDescriptionGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            DescriptionGridFormat(wptDescriptionGrid, GpxSource.WDescription);
        }

        private void wptAccuracyGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            AccuracyGridFormat(wptAccuracyGrid, GpxSource.WAccuracy);
        }

        private void trkDescriptionGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            DescriptionGridFormat(trkDescriptionGrid, GpxSource.TDescription);
        }

        private void trkAccuracyGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            AccuracyGridFormat(trkAccuracyGrid, GpxSource.TAccuracy);
        }

        private void rteDescriptionGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            DescriptionGridFormat(rteDescriptionGrid, GpxSource.RDescription);
        }

        private void rteAccuracyGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            AccuracyGridFormat(rteAccuracyGrid, GpxSource.RAccuracy);
        }
    }
}
